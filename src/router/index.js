import Vue from 'vue'
import Router from 'vue-router'

import Seaction from '@/components/Seaction'
import HomeContent from '@/components/HomeContent'
import AddContent from '@/components/AddContent'
import CollectList from '@/components/CollectList'
import News from '@/components/News'
import List from '@/components/List'
import Edit from '@/components/Edit'

Vue.use(Router)

export default new Router({
  mode: 'history',
  routes: [
    {
      path: '/',
      name: 'Seaction',
      component: Seaction,
      redirect: '/Seaction/List',
      children: [
        {
          path: '/Seaction/HomeContent',
          name: 'HomeContent',
          component: HomeContent
        },
        {
          path: '/Seaction/AddContent',
          name: 'AddContent',
          component: AddContent
        },
        {
          path: '/Seaction/CollectList',
          name: 'CollectList',
          component: CollectList
        },
        {
          path: '/Seaction/News',
          name: 'News',
          component: News
        },
        {
          path: '/Seaction/Edit',
          name: 'Edit',
          component: Edit
        },
        {
          path: '/Seaction/List',
          name: 'List',
          component: List
        }
      ]
    }
  ]
})
